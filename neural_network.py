# feed_forward.py - Implementation of a feed forward network and multiple layers.
import numpy as np

class NeuralNetwork:
	def __init__(self, layersInfo, errorFunction, optimizer):
		self.layers = layersInfo
		self.optimizer = optimizer
		self.errorFunction = errorFunction()

		# Check consistency
		outputSize = self.layers[0].outputSize
		for i in range(1, len(self.layers)):
			assert outputSize == self.layers[i].inputSize, "Wrong shape: output[l-1]: " + str(outputSize) \
				+ " vs. input[l]: " + str(self.layers[i].inputSize)
			outputSize = self.layers[i].outputSize

		self.inputSize = self.layers[0].inputSize
		self.outputSize = self.layers[len(self.layers) - 1].outputSize

	def forward(self, networkInput):
		raise NotImplementedError("Should have implemented this")

	def backward(self, networkInput, networkError):
		raise NotImplementedError("Should have implemented this")

	def train(self, datasetReader, epochsCount, batchSize, validateCheck, validateStep, validateStopAccuracy):
		raise NotImplementedError("Should have implemented this")

	def test(self, datasetReader, type):
		raise NotImplementedError("Should have implemented this")

	def optimize(self):
		for layer in self.layers:
			self.optimizer.optimize(layer)

	def printWeights(self):
		print("Weights:")
		for layer in self.layers:
			if type(layer.weights) != float:
				print(layer.__str__() + " Min:", np.min(layer.weights), "Max:", np.max(layer.weights), \
					"Mean:", np.mean(layer.weights))

	def __str__(self):
		Str = "Error function: " + self.errorFunction.__str__() + "\n"
		Str += "Optimizer: " + self.optimizer.__str__() + "\n"
		Str += "Layers: \n"
		for i in range(len(self.layers)):
			Str += str(i) + " " + self.layers[i].__str__() + "\n";
		return Str

class FeedForward(NeuralNetwork):
	def forward(self, networkInput):
		lastInput = networkInput
		for layer in self.layers:
			lastInput = layer.forward(lastInput)
		return lastInput

	def backward(self, networkInput, networkError):
		currentError = networkError
		# Traverse them backward
		for i in range(len(self.layers) - 1, 0, -1):
			currentLayer = self.layers[i]
			previousLayer = self.layers[i - 1]
			currentError = currentLayer.backward(previousLayer.layerOutput, currentError)
		self.layers[0].backward(networkInput, currentError)

	def train(self, datasetReader, epochsCount, batchSize, validateCheck=False, validateStep=0, \
		validateStopAccuracy=0):
		images, labels = datasetReader.getData("train")
		number_classes = datasetReader.getNumberOfClasses()
		num_images = len(images)

		for epoch in range(epochsCount):
			print("Epoch:", epoch)
			permutation = np.random.permutation(num_images)
			images = images[permutation]
			labels = labels[permutation]

			for i in range(0, len(permutation), batchSize):
				for batch_index in range(batchSize):
					networkInput = images[i + batch_index]
					label = labels[i + batch_index]

					labelVector = np.zeros((number_classes, 1))
					labelVector[label] = 1

					networkOutput = self.forward(networkInput)
					error = self.errorFunction.computeErrorMinimum(labelVector, networkOutput)
					self.backward(networkInput, error)
				self.optimize()

				# check accuracy every N steps and stop if it reaches that before all images are processed.
				if validateCheck and i > 0 and i % validateStep == 0:
					accuracy, error = self.test(datasetReader, "validate")
					print("Trained on", i, "images. Accuracy: %2.6f. Error: %2.3f" % (accuracy, error))
					if accuracy > validateStopAccuracy:
						return True

	def test(self, datasetReader, type):
		assert type in ("test", "validate")
		images, labels = datasetReader.getData(type)
		number_classes = datasetReader.getNumberOfClasses()
		assert len(images) == len(labels)

		permutation = np.random.permutation(len(images))
		images = images[permutation]
		labels = labels[permutation]

		correctCount = 0
		evaluateCount = len(images)
		# Save the results for error calculation afterwards.
		networkOutputs = np.zeros((evaluateCount, number_classes))
		correctLabels = np.zeros((evaluateCount, number_classes))
		for i in range(evaluateCount):
			image = images[i]
			label = labels[i]
			result = self.forward(image)

			# For error calculation. networkOutputs[i] = result gives random numpy error that should not give.
			networkOutputs[i] = result.reshape(networkOutputs[i].shape)
			correctLabels[i, label] = 1

			if np.argmax(result) == label:
				correctCount += 1

		accuracy = correctCount / evaluateCount
		error = self.errorFunction.computeCurrentError(correctLabels, networkOutputs)
		return accuracy, error

	def __str__(self):
		Str = "Feed Forward network. \n"
		Str += super().__str__()
		return Str


