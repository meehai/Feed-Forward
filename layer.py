# layer.py - Interface for a neural network layer and possible implementations of such layers.
import numpy as np
from transfer_function import *

# Layer interface used by a Neural Network 
class Layer:
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		self.inputSize = inputSize
		self.outputSize = outputSize
		self.transferFunction = transferFunction()

		# Output of this layer since the last step.
		self.layerOutput = np.zeros(self.outputSize)

		# Layer parameters, FullyConnected and Convolution updates the shape of this with correct shape.
		self.weights, self.biases, self.gradientWeights, self.gradientBiases = 0, 0, 0, 0

	# Compute the output of this layer based on the inputs
	def forward(self, layerInput):
		raise NotImplementedError("Should have implemented this")

	# Compute the derivative of the error w.r.t to internal parameters of the layer
	# Gradients are cummulated instead of modified on the spot
	def backward(self, layerInput, error):
		raise NotImplementedError("Should have implemented this")

	def __str__(self):
		return str(self.inputSize) + " -> " + str(self.outputSize)

# Fully Connected Layer. TODO: write comments
class FullyConnectedLayer(Layer):
	def __init__(self, inputSize, outputSize, transferFunction=Identity):
		super().__init__(inputSize, outputSize, transferFunction)

		# Layer's parameters, initialized using Xavier's initialization
		self.weights = np.random.normal(0, np.sqrt(2 / (inputSize[0] + outputSize[0])), \
			(inputSize[0], outputSize[0]))
		self.biases = np.random.normal(0, np.sqrt(2 / (inputSize[0] + outputSize[0])), outputSize)

		# Computed value
		self.layerOutput = np.zeros(outputSize)

		# Gradients
		self.gradientWeights = np.zeros((self.inputSize[0], self.outputSize[0]))
		self.gradientBiases = np.zeros(self.outputSize)

	# layerOutput is the Z(L) parameter in the functions and this is saved for backpropagation step
	# The forward step will return f(Z(L)) to the next layers.
	def forward(self, layerInput):
		self.layerOutput = np.dot(self.weights.T, layerInput) + self.biases
		return self.transferFunction.apply(self.layerOutput)

	def backward(self, layerInput, propagatedError):
		assert(propagatedError.shape == self.outputSize)

		# error is weights(L+1) * delta_y(L+1) => delta_Y(L) = error * f'(Z(L))
		delta_y = propagatedError * self.transferFunction.applyDerivative(self.layerOutput)

		# Compute the gradients w.r.t. the bias terms (self.g_biases)
		self.gradientBiases += delta_y

		# Compute the gradients w.r.t. the weights (self.g_weights)
		self.gradientWeights += np.dot(delta_y, layerInput.T).T

		# Compute and return the gradients w.r.t the inputs of this layer
		result = np.dot(self.weights, delta_y)
		return result

	def __str__(self):
		return "FullyConnected " + super().__str__() + " (" + self.transferFunction.__str__() + ")"

class SoftMaxLayer(Layer):
	def __init__(self, inputSize):
 		super().__init__(inputSize, inputSize)

	def forward(self, layerInput):
		coeff = np.sum(np.exp(layerInput))
		self.layerOutput = np.exp(layerInput) / coeff
		return self.layerOutput

	def backward(self, layerInput, error):
		Z = np.sum(self.layerOutput * error)
		return self.layerOutput * (error - Z)

	def __str__(self):
		return "SoftMax " + super().__str__()

class ReshapeLayer(Layer):
	def __init__(self, inputSize, outputSize):
		super().__init__(inputSize, outputSize)

	def forward(self, layerInput):
		self.layerOutput = layerInput.reshape(self.outputSize)
		return self.layerOutput

	def backward(self, layerInput, propagatedError):
		return propagatedError.reshape(self.inputSize)

	def __str__(self):
		return "Reshape " + super().__str__()

class ConvolutionalLayer(Layer):
	def __init__(self, inputSize, kernelSize, stride, numKernels, transferFunction=Identity):
		self.depth, self.width, self.height = inputSize
		assert (self.height - kernelSize) % stride == 0 and (self.width - kernelSize) % stride == 0
		self.kernelSize = kernelSize
		self.stride = stride
		self.numKernels = numKernels

		self.heightOutput = int((self.height - kernelSize) / stride + 1)
		self.widthOutput = int((self.width - kernelSize) / stride + 1)
		super().__init__(inputSize, (self.numKernels, self.widthOutput, self.heightOutput), transferFunction)

		# Layer's parameters
		self.weights = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), \
			(numKernels, self.depth, kernelSize, kernelSize))
		self.biases = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), (numKernels, 1))

		# Computed value
		self.layerOutput = np.zeros(self.outputSize)

		# Gradients
		self.gradientWeights = np.zeros((self.numKernels, self.depth, self.kernelSize, self.kernelSize))
		self.gradientBiases = np.zeros((self.numKernels, 1))

	def forward(self, layerInput):
		# TODO: think of mgrid.
		for i in range(self.heightOutput):
			out_i = i * self.stride
			for j in range(self.widthOutput):
				out_j = j * self.stride
				# A is the little portion of the image (on all dimensions) that the kernel is applied to
				A = layerInput[:, out_i : out_i + self.kernelSize, out_j : out_j + self.kernelSize]
				res = np.sum(A * self.weights, axis=(1,2,3)) + self.biases.reshape(self.numKernels)
				self.layerOutput[:, i, j] = res

		# Apply the trasnfer function to the output
		return self.transferFunction.apply(self.layerOutput)
	
	def forward_6for(self, layerInput):
		for n in range(self.numKernels):
			for m in range(self.depth):
				for y in range(0, self.height - self.kernelSize + 1, self.stride):
					k_y = int(y / self.stride)
					for x in range(0, self.width - self.kernelSize + 1, self.stride):
						k_x = int(x / self.stride)
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								a = layerInput[m, x+p, y+q] * self.weights[n, m, p, q]
								self.layerOutput[n, k_x, k_y] += a
			for k_y in range(self.heightOutput):
				for k_x in range(self.widthOutput):
					self.layerOutput[n, k_x, k_y] += self.biases[n]

		return self.transferFunction.apply(self.layerOutput)

	def backward(self, layerInput, propagatedError):
		# delta_Y is the actual error after deriving the forward's output with the trasnfer function. This is
		#  equivalent to having a TanH layer that translates the error from previous step but inside the conv layer.
		delta_y = propagatedError * self.transferFunction.applyDerivative(self.layerOutput)
		result = np.zeros(layerInput.shape)

		# Gradient w.r.t weights
		for out_i in range(self.heightOutput):
			in_i = out_i * self.stride
			for out_j in range(self.widthOutput):
				in_j = out_j * self.stride
				A_weights = layerInput[:, in_i : in_i + self.kernelSize, in_j : in_j + self.kernelSize]
				A_delta = result[:, in_i : in_i + self.kernelSize, in_j : in_j + self.kernelSize]
				b = delta_y[:, out_i, out_j]

				for n in range(self.numKernels):
					self.gradientWeights[n, :, :, :] += A_weights * b[n]
					A_delta[:, :] += self.weights[n, :, :, :] * b[n]

		self.gradientBiases += np.sum(delta_y, axis=(1,2)).reshape((-1, 1))
		return result

	def backward_6for(self, layerInput, propagatedError):
		delta_y = propagatedError * self.transferFunction.applyDerivative(self.layerOutput)

		# Gradient w.r.t weights
		for n in range(self.numKernels):
			for m in range(self.depth):
				for i in range(self.heightOutput):
					for j in range(self.widthOutput):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								a = layerInput[m, i*self.stride+p, j*self.stride+q] * delta_y[n, i, j]
								self.gradientWeights[n, m, p, q] += a

		# Gradient w.r.t inputs
		result = np.zeros(layerInput.shape)
		for n in range(self.numKernels):
			for m in range(self.depth):
				for i in range(self.height):
					for j in range(self.width):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								out_i = int((i - p) / self.stride)
								out_j = int((j - q) / self.stride)
								if ((i - p) % self.stride == 0) and ((j - q) % self.stride == 0) and (i - p >= 0) and (j - q >= 0) and out_i in range(0, self.heightOutput) and out_j in range(0, self.widthOutput):
									a = self.weights[n, m, p, q] * delta_y[n, out_i, out_j]
									result[m, i, j] += a

		# Alternative way of writing this without caring about the fraction
		'''
		# Gradient w.r.t inputs
		result = np.zeros(layerInput.shape)
		for n in range(self.numKernels):
			for m in range(self.depth):
				for out_i in range(self.heightOutput):
					for out_j in range(self.widthOutput):
						for p in range(self.kernelSize):
							for q in range(self.kernelSize):
								in_i = out_i * self.stride + p
								in_j = out_j * self.stride + q								
								a = self.weights[n, m, p, q] * delta_y[n, out_i, out_j]
								result[m, in_i, in_j] += a
		'''


		for n in range(self.numKernels):
			self.gradientBiases[n] += np.sum(delta_y[n])

		return result

	def __str__(self):
		return "Convolution " + super().__str__() + " (" + self.transferFunction.__str__() + ")"

class MaxPoolLayer(Layer):
	def __init__(self, inputSize, stride):
		self.depth, self.width, self.height = inputSize
		assert(self.height % stride == 0 and self.width % stride == 0)
		self.widthOutput = int(self.width / stride)
		self.heightOutput = int(self.height / stride)
		super().__init__(inputSize, (self.depth, self.widthOutput, self.heightOutput))

		self.stride = stride
		self.maxIndexes = {}

	def forward(self, layerInput):
		for m in range(self.depth):
			for i in range(self.heightOutput):
				for j in range(self.widthOutput):
					A = layerInput[m, i * self.stride : (i + 1) * self.stride, j * self.stride : (j + 1) * self.stride]
					out_i, out_j = np.array(np.unravel_index(A.argmax(), A.shape))
					self.layerOutput[m, i, j] = np.max(A)
					self.maxIndexes[(m, out_i + i * self.stride, out_j + j * self.stride)] = (m, i, j)
		return self.layerOutput

	def backward(self, layerInput, propagatedError):
		result = np.zeros(self.inputSize)
		for key, value in self.maxIndexes.items():
			result[key] = propagatedError[value]
		return result

	def __str__(self):
		return "MaxPool " + super().__str__()
