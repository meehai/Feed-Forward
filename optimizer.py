# optimizer.py - Different optimizer implementations (gradient descent only, for the moment)
class Optimizer:
	def __init__(self, learningRate):
		self.learningRate = learningRate

	# Some layers have no optimizers becasue they have no parameters.
	def optimize(self, layer):
		raise NotImplementedError("Should have implemented this")

	def __str__(self):
		raise NotImplementedError("Should have implemented this")

class GradientDescent(Optimizer):
	def optimize(self, layer):

		layer.weights -= self.learningRate * layer.gradientWeights
		layer.biases -= self.learningRate * layer.gradientBiases

		layer.gradientWeights *= 0
		layer.gradientBiases *= 0

	def __str__(self):
		return "Gradient Descent. Learning rate: " + str(self.learningRate)