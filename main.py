import sys, os
sys.path.append(os.path.dirname(__file__)) # Windows
import numpy as np
from datetime import datetime
from argparse import ArgumentParser
from sklearn.externals import joblib
from functools import partial

from neural_network import FeedForward
from transfer_function import *
from layer import *
from optimizer import *
from error import *
from utilities.public_api import normalizeData, MNISTReader, Cifar10Reader

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("--type", type=str, default="train")
	parser.add_argument("--dataset", type=str, default="mnist", help="Which dataset to test/train on")
	parser.add_argument("--dataset_path", type=str, default=".")
	parser.add_argument("--train_type", type=str, default="iterative")
	parser.add_argument("--epochs_count", type=int, default=2)
	parser.add_argument("--batch_size", type=int, default=1)
	parser.add_argument("--learning_rate", type=float, default=0.001)
	parser.add_argument("--validate_step", type=int, default=200)
	parser.add_argument("--validate_percent", type=float, default=5)
	parser.add_argument("--stop_accuracy", type=float, default=0.9, help="Stop learning once it reaches this accuracy")
	parser.add_argument("--file", type=str, default="nn.pkl", help="File name used for import/export of a neural " +
		"network")
	args = parser.parse_args()

	assert(args.validate_step % args.batch_size == 0)
	assert(args.dataset in ("mnist", "cifar10"))
	assert(args.type in ("train", "test"))
	assert(args.train_type in ("iterative"))

	return args

def train(args, feed_forward, dataset):
	assert(args.type == "train")
	# Just this for the moment, will probably move it in Network class later.
	return iterativeGraidentDescent(args, feed_forward, dataset)

def main():
	np.seterr(all="raise")
	args = parseArguments()
	datasetReader = MNISTReader(args.dataset_path, desiredShape=(1, 32, 32,)) if args.dataset == "mnist" \
		else Cifar10Reader(args.dataset_path, desiredShape=(3, 32, 32))
	depth = 1 if args.dataset == "mnist" else 3

	# layers = [
	# 	ConvolutionalLayer((depth, 32, 32), kernelSize=5, stride=1, numKernels=6, transferFunction=Tanh),
	# 	MaxPoolLayer((6, 28, 28), stride=2),
	# 	ConvolutionalLayer((6, 14, 14), kernelSize=5, stride=1, numKernels=16, transferFunction=Tanh),
	# 	MaxPoolLayer((16, 10, 10), stride=2),
	# 	ConvolutionalLayer((16, 5, 5), kernelSize=5, stride=1, numKernels=120, transferFunction=Tanh),
	# 	ReshapeLayer((120, 1, 1), (120, 1)),
	# 	FullyConnectedLayer((120, 1), (10, 1), transferFunction=Tanh),
	# 	SoftMaxLayer((10, 1))
	# ]

	layers = [
		ConvolutionalLayer((depth, 32, 32), kernelSize=5, stride=1, numKernels=10, transferFunction=Tanh),
		ReshapeLayer((10, 28, 28), (28 * 28 * 10, 1)),
		FullyConnectedLayer((28 * 28 * 10, 1), (100, 1), Logistic),
		FullyConnectedLayer((100, 1), (10, 1), Identity),
		SoftMaxLayer((10, 1))
	]

	# layers = [
	# 	ReshapeLayer((deph, 32, 32), (32 * 32 * deph, 1)),
	# 	FullyConnectedLayer((32 * 32 * deph, 1), (100, 1), Logistic),
	# 	FullyConnectedLayer((100, 1), (10, 1), Identity),
	# 	SoftMaxLayer((10, 1))
	# ]

	if args.type == "train":
		feed_forward = FeedForward(layers, errorFunction=LogisticError, optimizer=GradientDescent(args.learning_rate))
		print(feed_forward)
	
		now = datetime.now()
		feed_forward.train(datasetReader, args.epochs_count, args.batch_size, validateCheck=True, \
			validateStep=args.validate_step, validateStopAccuracy=args.validate_percent)
		# feed_forward = train(args, feed_forward, datasetReader)
		print("Training took:", (datetime.now() - now))
		joblib.dump(feed_forward, args.file)
	else:
		feed_forward = joblib.load(args.file)
		print(feed_forward)
	
		now = datetime.now()
		accuracy, error = feed_forward.test(datasetReader, "test")
		print("Testing took:", (datetime.now() - now))
		print("Accuracy: %2.6f" % accuracy)

if __name__ == "__main__":
	main()
